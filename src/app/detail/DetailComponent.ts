import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { MyService } from '../my.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html'
})
export class DetailComponent implements OnInit, OnDestroy {

  subscriptionParam: Subscription;
  formGroup: FormGroup;

  constructor(private activatedRoute: ActivatedRoute,
    private myService: MyService,
    private formBuilder: FormBuilder
  ) {

  }
  ngOnInit() {
    this.formGroup = this.formBuilder.group({      
        id: ['', Validators.required],
      name: ['', Validators.required]
    });

    this.subscriptionParam = this.activatedRoute.params.subscribe(
      params => {
        let id: number = params['id'];
        let user = this.myService.getUserById(id);
        if (user != null) {
          this.formGroup.setValue(user);
        }
      }
    );
  }

  ngOnDestroy(): void {
    console.log("Destroy");
    this.subscriptionParam.unsubscribe();
  }
}
