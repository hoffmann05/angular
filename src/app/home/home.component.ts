import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { MyService } from '../my.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  console = console;
  users: User[];
  selectedUser: any;
  constructor(
    private myService: MyService,
    private router: Router, ) {
    this.users = myService.getUsers();
  }

  ngOnInit() {
  }

  onRowSelect(event) {
    console.log(this.selectedUser);
    this.router.navigate(['/detail/' + this.selectedUser.id]);

  }

}
