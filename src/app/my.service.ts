import { Injectable } from '@angular/core';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class MyService {

  constructor() {

  }
  public getUsers(): User[] {

    return [{ id: 1, name: 'Bobby' },
    { id: 2, name: 'Sadie' },
    { id: 3, name: 'Hudson' },
    { id: 4, name: 'Bobby4' },
    { id: 5, name: 'Bobby5' },
    { id: 6, name: 'Bobby6' },
    { id: 7, name: 'Bobby7' },
    { id: 12, name: 'Keever' },
    { id: 23, name: 'Bobby23' },
    { id: 34, name: 'Bobby34' },
    { id: 45, name: 'Bobby45' },
    { id: 56, name: 'Bobby56' },
    { id: 67, name: 'Bobby67' },
    ];
  }

  public getUserById(id: number): User {
    return this.getUsers().find(user => user.id == id);
  }
}
